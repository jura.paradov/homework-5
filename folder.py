from yat.model import *


class ConstantFolder:
    def visit(self, tree):
        return tree.visit(self)

    def visit_number(self, expr):
        return expr

    def visit_function_definition(self, expr):
        new_func_body = list(map(self.visit, expr.function.body))
        new_func = Function(expr.function.args, new_func_body)
        return FunctionDefinition(expr.name, new_func)

    def visit_conditional(self, expr):
        new_condition = self.visit(expr.condition)

        def change_list(sentence_list):
            if sentence_list is not None:
                return list(map(self.visit, sentence_list))
            else:
                return None

        new_true = change_list(expr.true)
        new_false = change_list(expr.false)
        return Conditional(new_condition, new_true, new_false)

    def visit_print(self, expr):
        return Print(self.visit(expr.expr))

    def visit_read(self, expr):
        return expr

    def visit_function_call(self, expr):
        new_args = list(map(self.visit, expr.args))
        return FunctionCall(expr.fun_expr, new_args)

    def visit_reference(self, expr):
        return expr

    def visit_binary_operation(self, expr):
        new_rhs = self.visit(expr.rhs)
        new_lhs = self.visit(expr.lhs)
        if type(new_lhs) == Number:
            if type(new_rhs) == Number:
                return BinaryOperation(new_lhs, expr.op, new_rhs).evaluate(Scope())
            if new_lhs.value == 0 and expr.op == '*' and type(new_rhs) == Reference:
                return Number(0)
        else:
            if type(new_rhs) == Reference and type(new_lhs) == Reference:
                if new_lhs.name == new_rhs.name and expr.op == '-':
                    return Number(0)
            elif type(new_rhs) == Number and type(new_lhs) == Reference:
                if new_rhs.value == 0 and expr.op == '*':
                    return Number(0)
        return BinaryOperation(new_lhs, expr.op, new_rhs)

    def visit_unary_operation(self, expr):
        new_expr = self.visit(expr.expr)
        if type(new_expr) == Number:
            return UnaryOperation(expr.op, new_expr).evaluate(Scope())
        return UnaryOperation(expr.op, new_expr)
