class Scope:

    """Scope - представляет доступ к значениям по именам
    (к функциям и именованным константам).
    Scope может иметь родителя, и если поиск по имени
    в текущем Scope не успешен, то если у Scope есть родитель,
    то поиск делегируется родителю.
    Scope должен поддерживать dict-like интерфейс доступа
    (см. на специальные функции __getitem__ и __setitem__)
    """

    def __init__(self, parent=None):
        self.values = {}
        self.parent = parent

    def __setitem__(self, key, value):
        self.values[key] = value

    def __getitem__(self, key):
        if key in self.values:
            return self.values[key]
        else:
            return self.parent[key]


class Number:

    """Number - представляет число в программе.
    Все числа в нашем языке целые."""

    def __init__(self, value):
        self.value = int(value)

    def evaluate(self, _):
        return self

    def visit(self, visitor):
        return visitor.visit_number(self)


class Function:

    """Function - представляет функцию в программе.
    Функция - второй тип поддерживаемый языком.
    Функции можно передавать в другие функции,
    и возвращать из функций.
    Функция состоит из тела и списка имен аргументов.
    Тело функции это список выражений,
    т. е.  у каждого из них есть метод evaluate.
    Во время вычисления функции (метод evaluate),
    все объекты тела функции вычисляются последовательно,
    и результат вычисления последнего из них
    является результатом вычисления функции.
    Список имен аргументов - список имен
    формальных параметров функции."""

    def __init__(self, args, body):
        self.args = args
        self.body = body

    def evaluate(self, scope):
        result = []
        for x in self.body:
            result = x.evaluate(scope)
        return result


class FunctionDefinition:

    """FunctionDefinition - представляет определение функции,
    т. е. связывает некоторое имя с объектом Function.
    Результатом вычисления FunctionDefinition является
    обновление текущего Scope - в него
    добавляется новое значение типа Function."""

    def __init__(self, name, function):
        self.name = name
        self.function = function

    def evaluate(self, scope):
        scope[self.name] = self.function
        return self.function

    def visit(self, visitor):
        return visitor.visit_function_definition(self)


class Conditional:

    """
    Conditional - представляет ветвление в программе, т. е. if.
    """

    def __init__(self, condition, if_true, if_false=None):
        self.condition = condition
        self.true = if_true
        self.false = if_false

    def evaluate(self, scope):
        result = self.condition.evaluate(scope)
        body = self.true if result.value != 0 else self.false
        if body is not None:
            for x in body:
                result = x.evaluate(scope)
        return result

    def visit(self, visitor):
        return visitor.visit_conditional(self)


class Print:

    """Print - печатает значение выражения на отдельной строке."""

    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, scope):
        result = self.expr.evaluate(scope)
        print(result.value)
        return result

    def visit(self, visitor):
        return visitor.visit_print(self)


class Read:

    """Read - читает число из стандартного потока ввода
     и обновляет текущий Scope.
     Каждое входное число располагается на отдельной строке
     (никаких пустых строк и лишних символов не будет).
     """

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        result = Number(int(input()))
        scope[self.name] = result
        return result

    def visit(self, visitor):
        return visitor.visit_read(self)


class FunctionCall:

    """
    FunctionCall - представляет вызов функции в программе.
    В результате вызова функции должен создаваться новый объект Scope,
    являющий дочерним для текущего Scope
    (т. е. текущий Scope должен стать для него родителем).
    Новый Scope станет текущим Scope-ом при вычислении тела функции.
    """

    def __init__(self, fun_expr, args):
        self.fun_expr = fun_expr
        self.args = args

    def evaluate(self, scope):
        function = self.fun_expr.evaluate(scope)
        call_scope = Scope(scope)
        for arg_name, arg in zip(function.args, self.args):
            call_scope[arg_name] = arg.evaluate(scope)
        return function.evaluate(call_scope)

    def visit(self, visitor):
        return visitor.visit_function_call(self)


class Reference:

    """Reference - получение объекта
    (функции или переменной) по его имени."""

    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope[self.name]

    def visit(self, visitor):
        return visitor.visit_reference(self)


class BinaryOperation:

    """BinaryOperation - представляет бинарную операцию над двумя выражениями.
    Результатом вычисления бинарной операции является объект Number.
    Поддерживаемые операции:
    “+”, “-”, “*”, “/”, “%”, “==”, “!=”,
    “<”, “>”, “<=”, “>=”, “&&”, “||”."""

    def __init__(self, lhs, op, rhs):
        self.op = op
        self.lhs = lhs
        self.rhs = rhs

    def evaluate(self, scope):
        funcs = {
            '+': lambda x, y: x + y,
            '-': lambda x, y: x - y,
            '*': lambda x, y: x * y,
            '/': lambda x, y: x / y,
            '%': lambda x, y: x % y,
            '==': lambda x, y: x == y,
            '!=': lambda x, y: x != y,
            '<': lambda x, y: x < y,
            '>': lambda x, y: x > y,
            '<=': lambda x, y: x <= y,
            '>=': lambda x, y: x >= y,
            '&&': lambda x, y: x and y,
            '||': lambda x, y: x or y,
        }

        return Number(funcs[self.op](self.lhs.evaluate(scope).value, self.rhs.evaluate(scope).value))

    def visit(self, visitor):
        return visitor.visit_binary_operation(self)


class UnaryOperation:

    """UnaryOperation - представляет унарную операцию над выражением.
    Результатом вычисления унарной операции является объект Number.
    Поддерживаемые операции: “-”, “!”."""

    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def evaluate(self, scope):
        if self.op == "!":
            result = Number(not self.expr.evaluate(scope).value)
        else:
            result = Number(- self.expr.evaluate(scope).value)
        return result

    def visit(self, visitor):
        return visitor.visit_unary_operation(self)


def conditions():
    parent = Scope()
    printer = PrettyPrinter()
    parent["ten"] = Number(10)
    parent["if"] = Function(["a"],
                            [Conditional(BinaryOperation(Reference("ten"),
                                                         '!=',
                                                         Reference("a")),
                                         [Number(-1)],
                                         [Number(1337)])])

    func_def = FunctionDefinition("if", parent["if"])
    parent["false_result"] = FunctionCall(Reference("if"),
                                          [Reference("ten")])

    parent["not_ten"] = Number(134)
    parent["true_result"] = FunctionCall(Reference("if"),
                                         [Reference("not_ten")])
    cond = Conditional(BinaryOperation(Reference("ten"),
                                       '!=',
                                       Reference("not_ten")),
                       [Number(-1)],
                       [Number(1337)])
    printer.visit(func_def)
    printer.visit(parent["ten"])
    printer.visit(parent["false_result"])
    printer.visit(parent["not_ten"])
    printer.visit(parent["true_result"])
    printer.visit(cond)
    print("Test 'conditions' checked")


def func():
    printer = PrettyPrinter()
    f_def = FunctionDefinition("foo",
                               Function(['a', 'b'],
                                        [Print(Reference('b')),
                                         Print(Reference('a'))]))
    printer.visit(f_def)

    in1 = Read("num1")
    printer.visit(in1)
    in2 = Read("num2")
    printer.visit(in2)

    f_call = FunctionCall(Reference("foo"),
                          [Reference('num1'), Reference('num2')])
    printer.visit(f_call)
    printer.visit(in2)
    print("Test 'func' checked")


def constant():
    parent = Scope()
    printer = PrettyPrinter()
    const_folder = ConstantFolder()

    parent["ninety_seven"] = Number(97)
    parent["eighty_three"] = Number(83)
    op1 = BinaryOperation(Reference("ninety_seven"), '-', Reference("ninety_seven"))
    printer.visit(const_folder.visit(op1))

    op2 = BinaryOperation(BinaryOperation(Number(83), '+', Reference("eighty_three")), '*', Number(0))
    printer.visit(const_folder.visit(op2))

    op3 = BinaryOperation(BinaryOperation(BinaryOperation(Number(42), '*', Number(42)),
                                          '/',
                                          UnaryOperation('-', Number(1337))),
                          '*',
                          BinaryOperation(Number(42),
                                          '*',
                                          BinaryOperation(Reference("ninety_seven"),
                                                          '-',
                                                          Reference('ninety_seven'))))

    printer.visit(const_folder.visit(op3))
    printer.visit(op3)
    op4 = BinaryOperation(BinaryOperation(UnaryOperation('-', Reference('ninety_seven')),
                                          '/',
                                          BinaryOperation(Reference("ninety_seven"), '%', Reference('eighty_three'))),
                          '+',
                          BinaryOperation(Reference("ninety_seven"),
                                          '*',
                                          UnaryOperation('-',
                                                         Number(97))))
    printer.visit(const_folder.visit(op4))
    print("Test 'constant' checked")
