class PrettyPrinter:

    def __init__(self):
        self.tabs = 0

    def visit(self, tree):
        tree.visit(self)
        print(';')

    def print_expr_list(self, expr_list):
        print('{')
        self.tabs += 1
        for x in (expr_list or []):
            print('\t' * self.tabs, end='')
            x.visit(self)
            print(';')
        self.tabs -= 1
        print('\t' * self.tabs, '}', sep='', end='')

    def print_args(self, args):
        if len(args) > 0:
            args[0].visit(self)
            for i in range(1, len(args)):
                print(', ', end='')
                args[i].visit(self)

    def visit_number(self, expr):
        print('(', expr.value, ')', sep='', end='')

    def visit_function_definition(self, expr):
        print("def ", expr.name, '(', sep='', end='')
        print(', '.join(expr.function.args), end='')
        print(')', end='')
        self.print_expr_list(expr.function.body)

    def visit_conditional(self, expr):
        print('if (', end='')
        expr.condition.visit(self)
        print(') ', end='')
        self.print_expr_list(expr.true)
        print(' else ', end='')
        self.print_expr_list(expr.false)

    def visit_print(self, expr):
        print('print', end=' ')
        expr.expr.visit(self)

    def visit_read(self, expr):
        print('read', expr.name, sep=' ', end='')

    def visit_function_call(self, expr):
        expr.fun_expr.visit(self)
        print('(', end='')
        self.print_args(expr.args)
        print(')', end='')

    def visit_reference(self, expr):
        print(expr.name, end='')

    def visit_binary_operation(self, expr):
        print('(', end='')
        expr.lhs.visit(self)
        print(expr.op, end='')
        expr.rhs.visit(self)
        print(')', end='')

    def visit_unary_operation(self, expr):
        print('(', end='')
        print(expr.op, end='')
        expr.expr.visit(self)
        print(')', end='')
